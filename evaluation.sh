#!/bin/sh

# SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
# SPDX-FileCopyrightText: 2021 RWTH Aachen University
#
# SPDX-License-Identifier: LicenseRef-BEER-WARE-LICENSE

# This script really does not do anything special...
cat dataset/data-a.csv
cat dataset/data-b.csv

