<!--
SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
SPDX-FileCopyrightText: 2021 RWTH Aachen University

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# REUSE.Software License Validation Template

This is a simple template to demonstrate (software) license validation using the [REUSE.Software specification](https://reuse.software/). While the specification was intended to be used for software it is well applicable to other kinds of data sets.

This example contains several example code files that are for demonstration purposes only. The actual license validation is done using a GitLab CI job, that checks if the contents of the repository comply to the specification on every change.

To comply to the specification you roughly need two pieces (for full details see the [specification](https://reuse.software/spec/)):
* License Files: all licenses used must be stored in a directory `LICENSES`
* A License Header or a `.license` file (or a file `.reuse/dep5` for directories where adding a lot of license information is undesirable)

A license header or `.license` file should roughly look like this (depinding on the file format / programming language used):

```sh
# SPDX-FileCopyrightText: 2021 Marius Politze <politze@itc.rwth-aachen.de>
# SPDX-FileCopyrightText: 2021 RWTH Aachen University
# 
# SPDX-License-Identifier: CC-BY-SA-4.0
```

Fun fact: this repository itself aims to be comply to the [REUSE.Software specification](https://reuse.software/). Several licenses are used for different portions of the repository:

* [`README.md`](./README.md) (this file) uses CC-BY-SA-4.0, defined in a license header comment
* [`evaluation.sh`](./evaluation.sh) uses a custom "proprietary" `BEER-WARE-LICENSE` as in the [`LICENSEs`](./LICENSES) directory
* [`images/*`](./images) uses CC0-1.0 defined by `.license` files
* [`dataset/*`](./dataset) uses CC0-1.0 defined in the file [`.reuse/dep5`](./.reuse/dep5)

You can see that by looking at the CI pipelines of the `main` branch (or by the green check mark above the files view)

A demo merge request that would "break" compliance can be seen here: !2

## Quickstart

1. Create a fork of this repository or copy all the files to a new repository
1. Add a license of your choosing e.g. see https://choosealicense.com/non-software/
    * Update `LICENSE` file
1. Add your source code or other data
1. Make sure your repository respects the REUSE.Software specification

The CI job pipelines will always contain a small report if your files are matching the specification. If not the pipelines will report a "fail" status.

## How This Works in Detail

This repository uses the GitLab CI to run the REUSE.Software `lint` application.

## About REUSE.Software

The [REUSE.Software specification](https://reuse.software/) specification was created by the Free Software Foundation Europe e.V. to provide a set of recommendations for making licensing of Free Software easier. While this was intended to be used for software, the practices can be applied nicely to research data or data in general.

Under the hood REUSE.Software uses license information according to the [SPDX Specification](https://spdx.org/specifications). 

If you want to use the REUSE.Software Specification for your project, you might want to consider installing the [reuse-tool](https://github.com/fsfe/reuse-tool), a small python application, that helpy with some related tasks like downloading the licenses to the right places or checking compliance to the specification. 

## About GitLab CI

GitLab CI allows you to define workflows that react on events within the git repository (e.g. a change being pushed or a merge request being created). GitLab CI jobs are executed on a separate computer (or server) by installing a GitLab Runner. Fortunately, GitLab.com offers a set of "shared" runners that can be readily used by projects hosted there.

The workflows are configured in the file [`.gitlab-ci.yml`](.gitlab-ci.yml). We use this to run a small docker container provided by the REUSE.Software project that contains the [reuse-tool](https://github.com/fsfe/reuse-tool). See the [`.gitlab-ci.yml`](.gitlab-ci.yml) file for more details on that.
